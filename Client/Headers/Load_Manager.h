#pragma once

_BEGIN(Client)

class CLoad_Manager final: public CBase
{
	_DECLARE_SINGLETON(CLoad_Manager)
private:
	explicit CLoad_Manager();
	virtual ~CLoad_Manager() = default;
public:
	HRESULT Load_PlaneData();
	map<_uint, PLANEINFO*>* GetPlaneData() { return &m_mapPlaneData; }
private:
	map<_uint, PLANEINFO*> m_mapPlaneData;
	typedef map<_uint, PLANEINFO*> MAPPLANEDATA;

	// CBase을(를) 통해 상속됨
	virtual void Free() override;
};

_END