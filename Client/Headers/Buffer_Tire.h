#pragma once
#include "VIBuffer.h"

_BEGIN(Client)

class CBuffer_Tire final : public CVIBuffer
{
private:
	explicit CBuffer_Tire(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuffer_Tire(const CBuffer_Tire& rhs);
	virtual ~CBuffer_Tire() = default;
public:
	HRESULT Ready_VIBuffer();
	void Render_VIBuffer();
public:
	static CBuffer_Tire* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component();

private:
	virtual void Free();

public:
	void UpdateVertex();
private:
	void Ready_Index();


public:
	void SetUp_BufferPlayer(_float& _fX, _float& _fY);

	
private:
	_float fX = 0.f;
	_float fY = 0.f;




};

_END