#pragma once
#include "Component.h"

_BEGIN(Client)

class CPlane final : public CComponent
{
private:
	explicit CPlane(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CPlane(const CPlane& rhs); 
	virtual ~CPlane() = default;

public:
	HRESULT Ready_Plane();
	_bool Set_Plane(const _uint* PlaneNum);
	PLANEINFO* Find_PlaneInfo(const _uint * PlaneNum);
private:
	map<_uint, PLANEINFO*>* m_pMapPlaneData;
	PLANEINFO m_CurPlane;
public:
	static CPlane* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent * Clone_Component();

private:
	CLoad_Manager* m_pLoad_Manager = nullptr;
protected:
	virtual void Free();
};

_END
