#include "stdafx.h"
#include "..\Headers\Player.h"
#include "Management.h"
#include "Buffer_Player.h"
#include "ObjMove.h"
#include "Collision.h"

_USING(Client)

CPlayer::CPlayer(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
	, m_pInput_Device(GET_INSTANCE(CInput_Device))
{
	ZeroMemory(&plane, sizeof(plane));
	m_pInput_Device->AddRef();
}

CPlayer::CPlayer(const CPlayer & rhs)
	:CGameObject(rhs)
	, m_pInput_Device(rhs.m_pInput_Device)
{
	ZeroMemory(&plane, sizeof(plane));
	m_pInput_Device->AddRef();
}

HRESULT CPlayer::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CPlayer::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COLL_PLAYER, this)))
		return -1;
	

	m_pTransformCom->SetUp_Speed(5.0f, D3DXToRadian(90.f));
	m_pTransformCom->Scaling(2.f, 2.f, 2.f);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(5.f, 5.f, 0.f));

	///////////////////////////////////////////////////////
	plane[0] = { 0,10,10,{ 5.f,5.f,0.f },{ -1,1,4,3 } };
	plane[1] = { 1,10,10,{ 0.f,5.f,5.f },{ -1,2,4,0 } };
	plane[2] = { 2,10,10,{ 5.f,5.f,10.f },{ -1,6,4,1 } };
	plane[3] = { 3,10,10,{ 10.f,5.f,5.f },{ -1,0,4,5 } };
	plane[4] = { 4,10,10,{ 5.f,10.f,5.f },{ 0,1,2,3 } };

	plane[5] = { 5,10,10,{ 15.f,5.f,10.f },{ -1,3,9,8 } };
	plane[6] = { 6,10,10,{ 10.f,5.f,15.f },{ -1,11,9,2 } };
	plane[7] = { 7,10,10,{ 15.f,5.f,20.f },{ -1,8,9,6 } };
	plane[8] = { 8,10,10,{ 20.f,5.f,15.f },{ -1,5,9,13 } };
	plane[9] = { 9,10,10,{ 15.f,10.f,15.f },{ 5,6,10,8 } };

	plane[10] ={ 10,10,10,{15.f,15.f,20.f },{ 9,11,14,13 } };
	plane[11] ={ 11,10,20,{10.f,10.f,25.f },{ -1,12,14,6 } };
	plane[12] ={ 12,10,20,{15.f,10.f,30.f },{ -1,13,14,11 } };
	plane[13] ={ 13,10,20,{20.f,10.f,25.f },{ -1,8,14,12 } };
	plane[14] ={ 14,10,10,{15.f,20.f,25.f },{ 10,11,12,13 } };
	////////////////////////////////////////////////////////
	return NOERROR;
}

_int CPlayer::Update_GameObject(const _float & fTImeDelta)
{

	AsGravity(fTImeDelta);//중력에의한움직임
	KeyInput(fTImeDelta);//키인풋
	



	m_pObjMove->Setup_Obj(&plane[curPlane],m_pTransformCom);//값셋팅
	m_pObjMove->Update();//업데이트
	
	if (m_pObjMove->CheckIn(curPlane,prePlane))//평면이 바뀌진않았는지 체크
	{
		bCheck = true;
		moveCheck();		 //면이바뀜에따른 속도방향전환
	}
	m_pObjMove->Get_ClimbDir(climbDir);//올라온 벽 전해받기
	m_pObjMove->Get_TouchDir(touchDir,offX,offY);//터치방향과 오프셋전해받기



	

	return _int();
}

_int CPlayer::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (m_pRendererCom == nullptr)
		return -1;
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;


	return _int();
}

void CPlayer::Render_GameObject()
{
	if (m_pBufferCom == nullptr)
		return;


	m_pGraphic_Device->SetRenderState(D3DRS_ZENABLE, false);

	m_pTransformCom->SetUp_OnGraphicDev();


	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev()))
		return;

	if (!bCheck)
		m_pBufferCom->SetUp_BufferPlayer(&plane[curPlane], touchDir, climbDir, offX, offY);
	else
		bCheck = false;

	m_pBufferCom->Render_VIBuffer();

	m_pGraphic_Device->SetRenderState(D3DRS_ZENABLE, true);

}

void CPlayer::Coll_GameObject()
{
	
}

CPlayer * CPlayer::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CPlayer* pInstance = new CPlayer(pGraphic_Device);
	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CPlayer Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CPlayer::Clone_GameObject()
{
	CPlayer* pInstance = new CPlayer(*this);
	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CPlayer Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

HRESULT CPlayer::Ready_Component()
{
	CManagement* pManagement = GET_INSTANCE(CManagement);
	if (pManagement == nullptr)
		return E_FAIL;
	pManagement->AddRef();

	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (m_pTransformCom == nullptr)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Coll", m_pCollCom)))
		return E_FAIL;

	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Texture_Player");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;

	m_pBufferCom = (CBuffer_Player*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Buffer_Player");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	m_pObjMove = (CObjMove*)pManagement->Clone_Component(SCENE_STAGE, L"Component_ObjMove_Player");
	if (FAILED(Add_Component(L"Com_ObjMove", m_pObjMove)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

void CPlayer::KeyInput(const _float & fTImeDelta)
{
	

	if (m_pInput_Device->IsKeyPressing(DIK_UP))
		vSpd.y += 1.f;
	if (m_pInput_Device->IsKeyPressing(DIK_DOWN))
		m_pTransformCom->Go_Down(fTImeDelta);
	if (m_pInput_Device->IsKeyPressing(DIK_LEFT))
		vSpd.x = -2.f;
	if (m_pInput_Device->IsKeyPressing(DIK_RIGHT))
		vSpd.x = 2.f;
	


}

void CPlayer::AsGravity(const _float & fTImeDelta)
{
	m_pTransformCom->Move(fTImeDelta, vSpd);
	if (m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->y > 1.1f)
		vSpd.y -= 0.02f;
	else
		vSpd.y = 0.f;
	
	vSpd.x = 0.f;
}

void CPlayer::moveCheck()
{
	if (curPlane % 5 == 4)
		return;


	if (prePlane % 5 != 4)
		return;


	float fCur = 0.f;

	switch ((curPlane % 5)- climbDir)
	{
	case 1:
	case 3:
		fCur = vSpd.y;
		vSpd.y = vSpd.x;
		vSpd.x = -fCur;
		break;
	case -2:
	case 2:
		vSpd.y *= -1.f;
		break;
	case -1:
	case -3:
		fCur = vSpd.y;
		vSpd.y = -vSpd.x;
		vSpd.x = fCur;
		break;
	}

	

}

void CPlayer::Free()
{
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pInput_Device);
	Safe_Release(m_pObjMove);

	Safe_Release(m_pCollCom);
	Safe_Release(m_pCollCom);

	CGameObject::Free();
}
