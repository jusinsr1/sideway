#include "stdafx.h"
#include "..\Headers\Plane.h"

_USING(Client)

CPlane::CPlane(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CComponent(pGraphic_Device)
	,m_pLoad_Manager(GET_INSTANCE(CLoad_Manager))
{
	m_pLoad_Manager->AddRef();
}

CPlane::CPlane(const CPlane & rhs)
	:CComponent(rhs)
	,m_pLoad_Manager(rhs.m_pLoad_Manager)
{
	m_pLoad_Manager->AddRef();
}

HRESULT CPlane::Ready_Plane()
{
	m_pMapPlaneData = m_pLoad_Manager->GetPlaneData();
	return NOERROR;
}

_bool CPlane::Set_Plane(const _uint * PlaneNum)
{
	PLANEINFO* PlaneInfo = Find_PlaneInfo(PlaneNum);

	if (PlaneInfo == nullptr)
		return false;

	m_CurPlane = *PlaneInfo;

	return true;
}

PLANEINFO* CPlane::Find_PlaneInfo(const _uint * PlaneNum)
{
	auto& iter = m_pMapPlaneData->find(*PlaneNum);

	if (iter == m_pMapPlaneData->end())
		return nullptr;
	return iter->second;
}

CPlane * CPlane::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CPlane* pInstance = new CPlane(pGraphic_Device);
	if (FAILED(pInstance->Ready_Plane()))
	{
		_MSG_BOX("CPlane Created Failed")
		Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CPlane::Clone_Component()
{
	return new CPlane(*this);
}

void CPlane::Free()
{
	//Safe_Release(m_pLoad_Manager);
	CComponent::Free();
}
