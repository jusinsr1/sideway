
// MainFrm.cpp : CMainFrame 클래스의 구현
//

#include "stdafx.h"
#include "Tools.h"

#include "MainFrm.h"
#include "ToolsView.h"
#include "BaseForm.h"
#include "PopupWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

const int  iMaxUserToolbars = 10;
const UINT uiFirstUserToolBarId = AFX_IDW_CONTROLBAR_FIRST + 40;
const UINT uiLastUserToolBarId = uiFirstUserToolBarId + iMaxUserToolbars - 1;

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // 상태 줄 표시기
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

// CMainFrame 생성/소멸

CMainFrame::CMainFrame()
{
	// TODO: 여기에 멤버 초기화 코드를 추가합니다.
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	cs.cx = WINCX+400;
	cs.cy = WINCY;
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return TRUE;
}

// CMainFrame 진단

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif //_DEBUG


// CMainFrame 메시지 처리기

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	// 창 3개 분할
	m_MainSplt.CreateStatic(this, 1, 2);
	m_MainSplt.CreateView(0, 0, RUNTIME_CLASS(CToolsView), CSize(WINCX, WINCY), pContext);

	// SetColumnInfo(열 번호, 열의 크기, 허용가능한 최소 크기)
	m_MainSplt.SetColumnInfo(0, WINCX, 10);
	m_MainSplt.SetColumnInfo(1, 400, 10);

	m_SecondSplt.CreateStatic(&m_MainSplt, 2, 1, WS_CHILD | WS_VISIBLE,
		m_MainSplt.IdFromRowCol(0, 1));	// 부모 Splt의 0, 0 영역에 배치.

	m_SecondSplt.CreateView(0, 0, RUNTIME_CLASS(CBaseForm), CSize(400, 300), pContext);
	m_SecondSplt.CreateView(1, 0, RUNTIME_CLASS(CPopupWnd), CSize(400, 300), pContext);


	return TRUE;
}