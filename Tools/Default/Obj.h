#pragma once
class CObj
{
protected:
	explicit CObj(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CObj();

public:
	virtual void Ready_Object();
	virtual void Update() PURE;
	virtual	void Render() PURE;
	virtual void Release() PURE;
	virtual _vec3 UpdateRay(const _vec3* vMouseRay, const _vec3* vCameraPos, _float* dist);

public:
	enum STATE { STATE_RIGHT, STATE_UP, STATE_LOOK, STATE_POSITION, STATE_END };

	const _vec3* Get_StateInfo(STATE eState);
	void Set_StateInfo(STATE eState, const _vec3* pInfo);
	void SetUp_RotationX(const _float & fRadian);
	void SetUp_RotationY(const _float & fRadian);
	void SetUp_RotationZ(const _float & fRadian);
	void Rotation_Y(const _float& fTimeDelta);
	void Rotation_AxisRight(const _float& fTimeDelta);
	void Go_Straight();
	void Go_Left();
	void Go_Right();
	void Go_Up();
	void Go_Down();
	void BackWard();
	void Scaling(const _vec3 & Scale);
	_matrix Get_Matrix_Inverse() const;
	_vec3 Get_Scale();

protected:
	_matrix		m_matWorld;
	LPDIRECT3DDEVICE9			m_pGraphic_Device = nullptr;
};