#include "stdafx.h"
#include "SaveLoad_Manager.h"

IMPLEMENT_SINGLETON(CSaveLoad_Manager);

CSaveLoad_Manager::CSaveLoad_Manager()
{
}

CSaveLoad_Manager::~CSaveLoad_Manager()
{
}

void CSaveLoad_Manager::Add3DCube(Object3D ID, CObj * CObj)
{
	m_vec3D[ID].push_back(CObj);
}

void CSaveLoad_Manager::AddBaseTile(CObj* CObj)
{
	m_BaseTile = CObj;
}

void CSaveLoad_Manager::AddCamera(CCamera * CCamera)
{
	m_Camera = CCamera;
}

void CSaveLoad_Manager::AddSquare(Object2D ID, CObj * CObj)
{
	m_vec2D[ID].push_back(CObj);
}

void CSaveLoad_Manager::Update()
{
	if (m_BaseTile != nullptr)
		m_BaseTile->Update();
	if (m_Camera != nullptr)
		m_Camera->Update();
	for (int i = 0; i<Object3D::_3D_END; i++)
	{
		for (auto& Object : m_vec3D[i])
		{
			Object->Update();
		}
	}

	for (int i = 0; i<Object2D::_2D_END; i++)
	{
		for (auto& Object : m_vec2D[i])
		{
			Object->Update();
		}
	}
}

_vec3 CSaveLoad_Manager::PickCheck(POINT mouse)
{
	D3DVIEWPORT9		ViewPort;
	GET_INSTANCE(CDevice)->GetDevice()->GetViewport(&ViewPort);

	_vec4	vMousePos;
	vMousePos.x = mouse.x / (ViewPort.Width * 0.5f) - 1.f;
	vMousePos.y = mouse.y / ((ViewPort.Height * 0.5f) * -1.f) + 1.f;
	vMousePos.z = 0.0f;
	vMousePos.w = 1.f;

	_matrix	matProj;
	GET_INSTANCE(CDevice)->GetDevice()->GetTransform(D3DTS_PROJECTION, &matProj);
	D3DXMatrixInverse(&matProj, nullptr, &matProj);
	D3DXVec4Transform(&vMousePos, &vMousePos, &matProj);


	_vec3		vCameraPos(0.f, 0.f, 0.f);

	_vec3		vMouseRay = _vec3(vMousePos.x, vMousePos.y, vMousePos.z) - vCameraPos;

	_matrix	matView;
	GET_INSTANCE(CDevice)->GetDevice()->GetTransform(D3DTS_VIEW, &matView);

	D3DXMatrixInverse(&matView, nullptr, &matView);
	D3DXVec3TransformCoord(&vCameraPos, &vCameraPos, &matView);
	D3DXVec3TransformNormal(&vMouseRay, &vMouseRay, &matView);

	_vec3 Picking = { -1, -1, -1 };
	_float dist = 0;

	CObj* PickObj = nullptr;
	Picking = m_BaseTile->UpdateRay(&vMouseRay, &vCameraPos, &dist);
	if(Picking.x !=-1)
		PickObj = m_BaseTile;
	
	/*for (int i = 0; i<Object3D::_3D_END; i++)
	{
		for (auto& Object : m_vec3D[i])
		{
			Object->Render();
		}
	}
*/

	return Picking;
}

void CSaveLoad_Manager::Render()
{
	if (m_BaseTile != nullptr)
		m_BaseTile->Render();
	if (m_Camera != nullptr)
		m_Camera->Render();
	for (int i = 0; i<Object3D::_3D_END; i++)
	{
		for (auto& Object : m_vec3D[i])
		{
			Object->Render();
		}
	}

	for (int i = 0; i<Object2D::_2D_END; i++)
	{
		for (auto& Object : m_vec2D[i])
		{
			Object->Render();
		}
	}
}

void CSaveLoad_Manager::Release()
{
}
