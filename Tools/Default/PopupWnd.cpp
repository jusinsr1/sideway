// PopupWnd.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Tools.h"
#include "MySheet.h"
#include "PopupWnd.h"


// CPopupWnd

IMPLEMENT_DYNCREATE(CPopupWnd, CFormView)

CPopupWnd::CPopupWnd()
	: CFormView(IDD_POPUPWND)
	, m_pMySheet(nullptr)
{

}

CPopupWnd::~CPopupWnd()
{
}

void CPopupWnd::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPopupWnd, CFormView)
END_MESSAGE_MAP()


// CPopupWnd 진단입니다.

#ifdef _DEBUG
void CPopupWnd::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPopupWnd::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPopupWnd 메시지 처리기입니다.


void CPopupWnd::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (m_pMySheet == nullptr)
	{
		m_pMySheet = new CMySheet;
		m_pMySheet->Create(this, WS_CHILD | WS_VISIBLE);
		m_pMySheet->MoveWindow(0, 0, 1920, 1080);
	}

}
