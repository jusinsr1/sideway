// MapTool.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Tools.h"
#include "MapTool.h"
#include "afxdialogex.h"


// CMapTool 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMapTool, CPropertyPage)

CMapTool::CMapTool()
	: CPropertyPage(IDD_MAPTOOL)
	, m_ToolState(_T(""))
{

}

CMapTool::~CMapTool()
{
}

void CMapTool::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO2, m_Tex3DCombo);
	DDX_Text(pDX, IDC_EDIT1, _3DPosition.x);
	DDX_Text(pDX, IDC_EDIT3, _3DPosition.y);
	DDX_Text(pDX, IDC_EDIT4, _3DPosition.z);
	DDX_Text(pDX, IDC_EDIT5, _3DScale.x);
	DDX_Text(pDX, IDC_EDIT6, _3DScale.y);
	DDX_Text(pDX, IDC_EDIT7, _3DScale.z);
	DDX_Control(pDX, IDC_RADIO1, m_Type3DRadio[0]);
	DDX_Control(pDX, IDC_RADIO2, m_Type3DRadio[1]);
	DDX_Control(pDX, IDC_RADIO3, m_Type3DRadio[2]);
	DDX_Control(pDX, IDC_RADIO4, m_2DRadio);
	DDX_Text(pDX, IDC_EDIT8, m_ToolState);
}


BEGIN_MESSAGE_MAP(CMapTool, CPropertyPage)
	ON_BN_CLICKED(IDC_RADIO1, &CMapTool::OnBnClicked3DTypeChange)
	ON_BN_CLICKED(IDC_RADIO2, &CMapTool::OnBnClicked3DTypeChange)
	ON_BN_CLICKED(IDC_RADIO3, &CMapTool::OnBnClicked3DTypeChange)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_BUTTON1, &CMapTool::OnBnClickedDraw)
END_MESSAGE_MAP()


// CMapTool 메시지 처리기입니다.



BOOL CMapTool::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	m_pSaveLoad = GET_INSTANCE(CSaveLoad_Manager);
	m_Type3DRadio[0].SetCheck(true);
	m_2DRadio.SetCheck(true);

	m_ToolState.SetString(L"STATE_NORMAL");
	UpdateData(false);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}



void CMapTool::OnBnClicked3DTypeChange()
{
	for (size_t i = 0; i < 3; i++)
	{
		if (m_Type3DRadio[i].GetCheck())
			m_3DType == i;
	}
}




void CMapTool::OnBnClickedDraw()
{
	if (m_iMapToolState == STATE_NORMAL)
	{
		if (m_2DRadio.GetCheck())
			m_iMapToolState = STATE_DRAW2D;
		else
			m_iMapToolState = STATE_DRAW3D;
	}

	if (m_iMapToolState == STATE_NORMAL)
		m_ToolState.SetString(L"STATE_NORMAL");
	else if (m_iMapToolState == STATE_DRAW2D)
		m_ToolState.SetString(L"STATE_DRAW2D");
	else if (m_iMapToolState == STATE_DRAW3D)
		m_ToolState.SetString(L"STATE_DRAW3D");
	else if (m_iMapToolState == STATE_PICKING)
		m_ToolState.SetString(L"STATE_PICKING");
	UpdateData(TRUE);
}

void CMapTool::Update()
{
	if (m_iMapToolState == STATE_DRAW3D)
	{
		if(GET_INSTANCE(CInputDevice)->IsMouseDown)
			
	}
}
