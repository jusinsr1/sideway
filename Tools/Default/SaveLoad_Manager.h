#pragma once

#include "Obj.h"
#include "Camera.h"

class CObj;
class CCamera;
class CSaveLoad_Manager final
{
	DECLARE_SINGLETON(CSaveLoad_Manager);
	
public:
	enum Object3D { Building, Box, Spring, Others, _3D_END};
	enum Object2D { Monster, Item, Poster, CollisionBox, CameraBox, _2D_END };
private:
	explicit CSaveLoad_Manager();
	virtual ~CSaveLoad_Manager();
public:
	void Add3DCube(Object3D ID, CObj* CObj);
	void AddSquare(Object2D ID, CObj * CObj);


	void AddBaseTile(CObj* CObj);
	void AddCamera(CCamera* CObj);
	CCamera* GetCamera() { return m_Camera; }
	void Update();
	_vec3 PickCheck(POINT mouse);
	void Render();
	void Release();

private:
	CObj* m_BaseTile = nullptr;
	CCamera* m_Camera = nullptr;

	vector<CObj*> m_vec3D[_3D_END];
	vector<CObj*> m_vec2D[_2D_END];
	CObj* m_SelectObj = nullptr;
};

