#pragma once
#include "MapObject.h"
class CCube final:	public CMapObject
{
public:
	explicit CCube(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CCube();

	void Ready_Cube(_vec3 & Pos, _vec3 & Scale, wstring* Path = nullptr);
	virtual void Update() override;
	virtual void Render() override;
	virtual void Release() override;
	virtual _vec3 UpdateRay(const _vec3* vMouseRay, const _vec3* vCameraPos, _float* dist);
	void SetWorld();
public:
	static CCube* Create(LPDIRECT3DDEVICE9 pGraphic_Device, _vec3& Pos, _vec3& Scale, wstring* Path = nullptr);
private:
	_vec3 m_Scale = { 0,0,0 };
	_vec3 m_Position = { 0,0,0 };

	LPDIRECT3DCUBETEXTURE9 m_pCubeTexture;

};

