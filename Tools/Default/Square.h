#pragma once
#include "MapObject.h"
class CSquare :	public CMapObject
{
private:
	CSquare(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CSquare();

public:
	void Ready_Square(const _vec3 Scale, const _vec3 Pos, wstring* Path = nullptr);
	virtual void Update() override;
	virtual void Render() override;
	virtual void Release() override;
	virtual _vec3 UpdateRay(const _vec3* vMouseRay, const _vec3* vCameraPos, _float* dist);

	void matWorldChange();
public:
	static CSquare* Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _float Width, const _float Height, const _vec3 Pos, wstring* Path = nullptr);

protected:
	LPDIRECT3DTEXTURE9			m_pTexture = nullptr;
private:
	float m_fWidth = 0;
	float m_fHeight = 0;
	_vec3 m_vecScale = { 1,1,1 };
	_vec3 m_vecPos = { 0,0,0 };
};

