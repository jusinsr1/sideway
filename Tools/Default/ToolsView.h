
// ToolsView.h : CToolsView 클래스의 인터페이스
//

#pragma once

class CToolsDoc;
class CToolsView : public CView
{
protected: // serialization에서만 만들어집니다.
	CToolsView();
	DECLARE_DYNCREATE(CToolsView)

// 특성입니다.
public:
	CToolsDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual void OnDraw(CDC* pDC);  // 이 뷰를 그리기 위해 재정의되었습니다.
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 구현입니다.
public:
	virtual ~CToolsView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
private:
	LPDIRECT3DDEVICE9 m_pGraphic_Device = nullptr;
	POINT m_mouse;
protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	void BaseFormUpdate();
};

#ifndef _DEBUG  // ToolsView.cpp의 디버그 버전
inline CToolsDoc* CToolsView::GetDocument() const
   { return reinterpret_cast<CToolsDoc*>(m_pDocument); }
#endif

