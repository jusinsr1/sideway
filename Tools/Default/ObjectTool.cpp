// ObjectTool.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Tools.h"
#include "ObjectTool.h"
#include "afxdialogex.h"


// CObjectTool 대화 상자입니다.

IMPLEMENT_DYNAMIC(CObjectTool, CPropertyPage)

CObjectTool::CObjectTool()
	: CPropertyPage(IDD_OBJECTTOOL)
{

}

CObjectTool::~CObjectTool()
{
}

void CObjectTool::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CObjectTool, CPropertyPage)
END_MESSAGE_MAP()


// CObjectTool 메시지 처리기입니다.
