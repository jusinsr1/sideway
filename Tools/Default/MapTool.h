#pragma once
#include "afxwin.h"

// CMapTool 대화 상자입니다.

class CMapTool : public CPropertyPage
{
	DECLARE_DYNAMIC(CMapTool)

public:
	CMapTool();
	virtual ~CMapTool();

	// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MAPTOOL };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	enum ToolState { STATE_NORMAL, STATE_DRAW2D, STATE_DRAW3D, STATE_PICKING, STATE_END };
private:
	CSaveLoad_Manager* m_pSaveLoad = nullptr;
public:
	afx_msg void OnBnClicked3DTypeChange();
	afx_msg void OnBnClickedDraw();
	void Update();
public:
	CButton m_Type3DRadio[3];
	_uint m_3DType = 0;
	CComboBox m_Tex3DCombo;
	_vec3 _3DPosition = { 0,0,0 };
	_vec3 _3DScale = { 0,0,0 };

	CButton m_2DRadio;
	ToolState m_iMapToolState = STATE_NORMAL;
	CString m_ToolState;

	_vec3 m_DrawPosition[2];
};
