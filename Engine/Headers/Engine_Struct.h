#pragma once

typedef struct tagCamera_Desc
{
	D3DXVECTOR3		vEye;
	D3DXVECTOR3		vAt;
	D3DXVECTOR3		vAxisY;
}CAMERADESC;

typedef struct tagProjection_Camera_Desc
{
	float			fFovY;
	float			fAspect;
	float			fNear;
	float			fFar;
}PROJDESC;

typedef struct tagVertex_Texture
{
	D3DXVECTOR3			vPosition;
	D3DXVECTOR2			vTexUV;
}VTXTEX;


typedef struct tagVertexCube_Texture
{
	D3DXVECTOR3			vPosition;
	D3DXVECTOR3			vTexUV;
}VTXCUBETEX;

typedef struct tagParticle
{
	D3DXVECTOR3 vPosition;
	DWORD		dwColor;
}PARTICLE;

typedef struct tagAttibute
{
	D3DXVECTOR3 vPosition;
	D3DXVECTOR3	vVelocity;
	D3DXVECTOR3 vAcceleration;
	float		fTimeLife;
	float		fAge;
	DWORD		dwColor;
	DWORD		dwColorFade;
	bool		bIsLive;

}ATTIBUTE;

typedef struct tagVertex_Color
{
	D3DXVECTOR3			vPosition;
	DWORD				dwColor;
}VTXCOL;

//typedef struct tagVertex_Tire
//{
//	D3DXVECTOR3			vPosition;
//	D3DXVECTOR3			v_nPos;
//	DWORD				dwColor;
//}VTXTIRE;

typedef struct tagPolygon16
{
	unsigned short		_0, _1, _2;
}POLYGON16;

typedef struct tagPolygon32
{
	unsigned long		_0, _1, _2;
}POLYGON32;
