#pragma once
#include "Base.h"
#include "GameObject.h"

_BEGIN(Engine)

class CLayer final : public CBase
{
public:
	explicit CLayer();
	virtual ~CLayer() = default;
public:
	CComponent* Get_ComponentPointer(const _tchar* pComponentTag, const _uint& iIndex);
public:
	HRESULT Add_Object(CGameObject* pGameObject);
	HRESULT Ready_Layer();
	_int Update_Object(const _float& fTimeDelta);
	_int LastUpdate_Object(const _float& fTimeDelta);
private:
	list<CGameObject*>			m_ObjectList;
	typedef list<CGameObject*>	OBJECTLIST;
public:
	static CLayer* Create();
protected:
	virtual void Free();
};

_END