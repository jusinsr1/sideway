#pragma once

#include "VtxBuffer.h"

_BEGIN(Engine)

class _ENGINE_DLL CBuffer_Particle : public CVtxBuffer
{
protected:
	explicit CBuffer_Particle(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuffer_Particle(const CBuffer_Particle& rhs);
	virtual ~CBuffer_Particle() = default;
public:
	virtual HRESULT		SetUp_Particle(ATTIBUTE* pBute) PURE;
	virtual HRESULT		Reset_Particle();
	virtual HRESULT		Add_Particle();
	virtual _int		Update(const _float& fTimeDelta);	
	virtual void		Render_Particle(LPDIRECT3DTEXTURE9 pTexture);

public:
	_bool				Check_Empty();
	_bool				Check_Dead();

protected:
	virtual void		PreRender_Particle();
	virtual void		PostRender_Particle();
	DWORD				FtoDw(_float f)
	{
		return *((DWORD*)&f);
	}
protected:
	D3DXVECTOR3 m_vOrigin;
	_float		m_fEmitRate = 0.f , m_fSize = 0.f;
	_bool		m_bIsDead = false;
	_ulong		m_dwOffset = 0, m_dwBatchSize = 0;
protected:
	list<ATTIBUTE>			m_Particle_Lst;
	typedef list<ATTIBUTE>	PARTICLELST;
protected:
	void		Release_Dead_Particle();
protected:
	virtual void Free();


};

_END