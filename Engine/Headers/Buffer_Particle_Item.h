#pragma once
#include "Buffer_Particle.h"

_BEGIN(Engine)

class _ENGINE_DLL CBuffer_Particle_Item final :
	public CBuffer_Particle
{
protected:
	explicit CBuffer_Particle_Item(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuffer_Particle_Item(const CBuffer_Particle_Item& rhs);
	virtual ~CBuffer_Particle_Item() = default;
public:
	virtual	HRESULT	Ready_ParticleItem(const _uint iNumParticle);
	virtual HRESULT	SetUp_Particle(ATTIBUTE* pBute) override;
	virtual	_int	Update(const _float& fTimeDelta);
	virtual	void	Render_Particle(LPDIRECT3DTEXTURE9 pTexture = nullptr);

private:
	_float			m_fTime = 0.f;

public:
	static CBuffer_Particle_Item* Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _uint iNumParticle);
	virtual CComponent*	   Clone_Component();

protected:
	virtual	void			Free();
};

_END