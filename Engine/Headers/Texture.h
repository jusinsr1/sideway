#pragma once
#include "Component.h"

_BEGIN(Engine)

class _ENGINE_DLL CTexture final : public CComponent
{

public:
	enum TYPE { TYPE_GENERAL,TYPE_CUBE , TYPE_END };

public:
	const _uint GetTexSize() { return m_vecTexture.size(); }

private:
	explicit CTexture(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CTexture(const CTexture& rhs);
	virtual ~CTexture() = default;
public:
	HRESULT Ready_Texture(const _tchar* pFilePath, const _uint& iNumTexture, 
		const TYPE eTypeID);
	HRESULT SetUp_OnGraphicDev(const _uint& iIndex = 0);
private:
	vector<IDirect3DBaseTexture9*>			m_vecTexture;
	typedef vector<IDirect3DBaseTexture9*>	VECTEXTURE;
public:
	static CTexture* Create(LPDIRECT3DDEVICE9 pGraphic_Device, const TYPE eTypeID,
		const _tchar* pFilePath, const _uint& iNumTexture = 1);
		virtual CComponent* Clone_Component();
protected:
	virtual void Free();
};

_END