#pragma once
#include "Component.h"

_BEGIN(Engine)

class _ENGINE_DLL CVIBuffer abstract : public CComponent
{
protected:
	explicit CVIBuffer(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CVIBuffer(const CVIBuffer& rhs);
	virtual ~CVIBuffer() = default;
public:
	HRESULT Ready_VIBuffer();
protected:
	LPDIRECT3DVERTEXBUFFER9		m_pVB = nullptr;
	_uint						m_iStride = 0;
	_uint						m_iNumVertices;
	_ulong						m_dwFVF = 0;
protected:
	LPDIRECT3DINDEXBUFFER9		m_pIB = nullptr;
	_uint						m_iPolygonSize = 0;
	_uint						m_iNumPolygons = 0;
	D3DFORMAT					m_eFormat = D3DFORMAT(0);
protected:
	_vec3*						m_pPosition = nullptr;

public:
	virtual CComponent* Clone_Component() = 0;
protected:
	virtual void Free();
};

_END
