#include "..\Headers\Buffer_Cube.h"



CBuffer_Cube::CBuffer_Cube(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CVIBuffer(pGraphic_Device)
{
}

CBuffer_Cube::CBuffer_Cube(const CBuffer_Cube & rhs)
	: CVIBuffer(rhs)
{
}

HRESULT CBuffer_Cube::Ready_VIBuffer()
{
	m_iNumVertices = 8;
	m_iStride = sizeof(VTXCOL);
	m_dwFVF = D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1;

	m_iNumPolygons = 12;
	m_iPolygonSize = sizeof(POLYGON16);
	m_eFormat = D3DFMT_INDEX16;

	if (FAILED(CVIBuffer::Ready_VIBuffer()))
		return E_FAIL;

	VTXCOL*		pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);

	pVertices[0].vPosition =_vec3(-0.5f, 0.5f, -0.5f);
	pVertices[0].dwColor = D3DXCOLOR(0.f, 1.f, 1.f, 0.5f);
	pVertices[1].vPosition = _vec3(0.5f, 0.5f, -0.5f);
	pVertices[1].dwColor = D3DXCOLOR(0.f, 1.f, 1.f, 0.5f);
	pVertices[2].vPosition = _vec3(0.5f, -0.5f, -0.5f);
	pVertices[2].dwColor = D3DXCOLOR(0.f, 1.f, 1.f, 0.5f);
	pVertices[3].vPosition = _vec3(-0.5f, -0.5f, -0.5f);
	pVertices[3].dwColor = D3DXCOLOR(0.f, 1.f, 1.f, 0.5f);

	pVertices[4].vPosition =  _vec3(-0.5f, 0.5f, 0.5f);
	pVertices[4].dwColor = D3DXCOLOR(0.f, 1.f, 1.f, 0.5f);
	pVertices[5].vPosition =  _vec3(0.5f, 0.5f, 0.5f);
	pVertices[5].dwColor = D3DXCOLOR(0.f, 1.f, 1.f, 0.5f);
	pVertices[6].vPosition = _vec3(0.5f, -0.5f, 0.5f);
	pVertices[6].dwColor = D3DXCOLOR(0.f, 1.f, 1.f, 0.5f);
	pVertices[7].vPosition =  _vec3(-0.5f, -0.5f, 0.5f);
	pVertices[7].dwColor = D3DXCOLOR(0.f, 1.f, 1.f, 0.5f);

	m_pVB->Unlock();

	POLYGON16*	pIndices = nullptr;
	m_pIB->Lock(0, 0, (void**)&pIndices, 0);

	pIndices[0]._0 = 1;	pIndices[0]._1 = 5;	pIndices[0]._2 = 6;
	pIndices[1]._0 = 1;	pIndices[1]._1 = 6;	pIndices[1]._2 = 2;
	pIndices[2]._0 = 4;	pIndices[2]._1 = 0;	pIndices[2]._2 = 3;
	pIndices[3]._0 = 4;	pIndices[3]._1 = 3;	pIndices[3]._2 = 7;
	pIndices[4]._0 = 4;	pIndices[4]._1 = 5;	pIndices[4]._2 = 1;
	pIndices[5]._0 = 4;	pIndices[5]._1 = 1;	pIndices[5]._2 = 0;
	pIndices[6]._0 = 3;	pIndices[6]._1 = 2;	pIndices[6]._2 = 6;
	pIndices[7]._0 = 3;	pIndices[7]._1 = 6;	pIndices[7]._2 = 7;
	pIndices[8]._0 = 7;	pIndices[8]._1 = 6;	pIndices[8]._2 = 5;
	pIndices[9]._0 = 7;	pIndices[9]._1 = 5;	pIndices[9]._2 = 4;
	pIndices[10]._0 = 0; pIndices[10]._1 = 1; pIndices[10]._2 = 2;
	pIndices[11]._0 = 0; pIndices[11]._1 = 2; pIndices[11]._2 = 3;

	m_pIB->Unlock();

	return NOERROR;
}

void CBuffer_Cube::Render_VIBuffer()
{
	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, m_iStride);
	m_pGraphic_Device->SetFVF(m_dwFVF);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_iNumVertices, 0, m_iNumPolygons);
}

CBuffer_Cube * CBuffer_Cube::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CBuffer_Cube* pInstance = new CBuffer_Cube(pGraphic_Device);
	if (FAILED(pInstance->Ready_VIBuffer()))
	{
		_MSG_BOX("Buffer_RcTex Created Failed")
			Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CBuffer_Cube::Clone_Component()
{
	return new CBuffer_Cube(*this);
}

void CBuffer_Cube::Free()
{
	CVIBuffer::Free();
}

