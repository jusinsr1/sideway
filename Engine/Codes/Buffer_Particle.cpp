#include "..\Headers\Buffer_Particle.h"

CBuffer_Particle::CBuffer_Particle(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CVtxBuffer(pGraphic_Device)
{

}

CBuffer_Particle::CBuffer_Particle(const CBuffer_Particle & rhs)
	: CVtxBuffer(rhs),
	m_vOrigin(rhs.m_vOrigin),
	m_fEmitRate(rhs.m_fEmitRate),
	m_fSize(rhs.m_fSize),
	m_bIsDead(rhs.m_bIsDead),
	m_dwOffset(rhs.m_dwOffset), 
	m_dwBatchSize(rhs.m_dwBatchSize),
	m_Particle_Lst(rhs.m_Particle_Lst)
{

}

HRESULT CBuffer_Particle::Reset_Particle()
{
	for (auto& pBute : m_Particle_Lst)
		SetUp_Particle(&pBute);

	return NOERROR;
}

HRESULT CBuffer_Particle::Add_Particle()
{
	ATTIBUTE tBute;

	ZeroMemory(&tBute, sizeof(ATTIBUTE));

	SetUp_Particle(&tBute);

	m_Particle_Lst.push_back(tBute);

	return NOERROR;
}

_int CBuffer_Particle::Update(const _float & fTimeDelta)
{
	return 0;
}

void CBuffer_Particle::PreRender_Particle()
{
	//m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, false);
	m_pGraphic_Device->SetRenderState(D3DRS_POINTSPRITEENABLE, true);
	m_pGraphic_Device->SetRenderState(D3DRS_POINTSCALEENABLE, true);
	m_pGraphic_Device->SetRenderState(D3DRS_POSITIONDEGREE, true);
	m_pGraphic_Device->SetRenderState(D3DRS_POINTSIZE, FtoDw(m_fSize));
 	m_pGraphic_Device->SetRenderState(D3DRS_POINTSIZE_MIN, FtoDw(0.2f));
	m_pGraphic_Device->SetRenderState(D3DRS_POINTSCALE_A, FtoDw(0.f));
	m_pGraphic_Device->SetRenderState(D3DRS_POINTSCALE_B, FtoDw(0.f));
	m_pGraphic_Device->SetRenderState(D3DRS_POINTSCALE_C, FtoDw(1.f));

	
	m_pGraphic_Device->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	m_pGraphic_Device->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);

	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	m_pGraphic_Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pGraphic_Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVDESTALPHA);

}

void CBuffer_Particle::Render_Particle(LPDIRECT3DTEXTURE9 pTexture)
{
	if (!m_Particle_Lst.empty())
	{

		PreRender_Particle();

		if (nullptr != pTexture)
		m_pGraphic_Device->SetTexture(0, pTexture);
		
		m_pGraphic_Device->SetFVF(m_dwFVF);
		m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, sizeof(PARTICLE));

		if (m_dwOffset >= m_iNumVertices)
			m_dwOffset = 0;

		PARTICLE* pParticles = nullptr;

		m_pVB->Lock(m_dwOffset * sizeof(PARTICLE), m_iNumVertices * sizeof(PARTICLE),
			(void**)&pParticles, m_dwOffset ? D3DLOCK_NOOVERWRITE : D3DLOCK_DISCARD);

		// ex ? ex : ex  _ 삼항 연산자,

		_ulong dwNum_InBatch = 0;

		for (auto& rBute : m_Particle_Lst)
		{
			if (rBute.bIsLive)
			{
				// 파티클이 생존시, 다음 버텍스 버퍼 세그먼트에 복사

				pParticles->vPosition = rBute.vPosition;
				pParticles->dwColor = D3DXCOLOR(1.f, 1.f, 1.f, 1.f);

				pParticles++;
				dwNum_InBatch++;

				if (dwNum_InBatch == m_dwBatchSize)
				{
					m_pVB->Unlock();

					m_pGraphic_Device->DrawPrimitive(D3DPT_POINTLIST, m_dwOffset, m_dwBatchSize);

					m_dwOffset += m_dwBatchSize;

					if (m_dwOffset >= m_iNumVertices)
						m_dwOffset = 0;

					m_pVB->Lock(m_dwOffset * sizeof(PARTICLE), m_iNumVertices * sizeof(PARTICLE),
						(void**)&pParticles, m_dwOffset ? D3DLOCK_NOOVERWRITE : D3DLOCK_DISCARD);


					dwNum_InBatch = 0;
				}
			}
		}

		m_pVB->Unlock();


		if (dwNum_InBatch)
			m_pGraphic_Device->DrawPrimitive(D3DPT_POINTLIST, m_dwOffset, m_dwBatchSize);
		
		m_dwOffset += m_dwBatchSize; 

		PostRender_Particle();
	}
		

}

void CBuffer_Particle::PostRender_Particle()
{
	//m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, false);
	m_pGraphic_Device->SetRenderState(D3DRS_POINTSPRITEENABLE, false);
	m_pGraphic_Device->SetRenderState(D3DRS_POINTSCALEENABLE, false);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, false);


	
}

_bool CBuffer_Particle::Check_Empty()
{
	return m_Particle_Lst.empty();
}

_bool CBuffer_Particle::Check_Dead()
{
	return m_bIsDead;
}


void CBuffer_Particle::Release_Dead_Particle()
{
	for (auto iter = m_Particle_Lst.begin();
		iter != m_Particle_Lst.end();)
	{
		if (!iter->bIsLive)
			iter = m_Particle_Lst.erase(iter);
		else
			iter++;
	}
}

void CBuffer_Particle::Free()
{
	m_Particle_Lst.clear();

	CVtxBuffer::Free();
}
