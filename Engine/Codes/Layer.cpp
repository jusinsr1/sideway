#include "..\Headers\Layer.h"



CLayer::CLayer()
{
}

CComponent * CLayer::Get_ComponentPointer(const _tchar * pComponentTag, const _uint & iIndex)
{
	if (m_ObjectList.size() <= iIndex)
		return nullptr;

	auto	iter = m_ObjectList.begin();

	for (size_t i = 0; i < iIndex; ++i)
		++iter;

	return (*iter)->Get_ComponentPointer(pComponentTag);
}

HRESULT CLayer::Add_Object(CGameObject * pGameObject)
{
	if (pGameObject == nullptr)
		return E_FAIL;

	m_ObjectList.push_back(pGameObject);

	return NOERROR;
}

HRESULT CLayer::Ready_Layer()
{
	return NOERROR;
}

_int CLayer::Update_Object(const _float & fTimeDelta)
{
	for (auto& CGameObject : m_ObjectList)
	{
		if (CGameObject != nullptr)
		{
			if (CGameObject->Update_GameObject(fTimeDelta) & 0x80000000)
				return -1;
		}
	}
	return _int();
}

_int CLayer::LastUpdate_Object(const _float & fTimeDelta)
{
	for (auto& CGameObject : m_ObjectList)
	{
		if (CGameObject != nullptr)
		{
			if (CGameObject->LastUpdate_GameObject(fTimeDelta) & 0x80000000)
				return -1;
		}
	}
	return _int();
}

CLayer * CLayer::Create()
{
	CLayer* pInstance = new CLayer();

	if (FAILED(pInstance->Ready_Layer()))
	{
		_MSG_BOX("CLayer Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CLayer::Free()
{
	for (auto& pGameObject : m_ObjectList)
		Safe_Release(pGameObject);
	m_ObjectList.clear();
}

