#include "..\Headers\VtxBuffer.h"



CVtxBuffer::CVtxBuffer(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CComponent(pGraphic_Device)
{
}

CVtxBuffer::CVtxBuffer(const CVtxBuffer & rhs)
	:CComponent(rhs),
	m_dwFVF(rhs.m_dwFVF),
	m_iNumVertices(rhs.m_iNumVertices),
	m_pVB(rhs.m_pVB)
{
}

HRESULT CVtxBuffer::Ready_CVtxBuffer(const _uint iNumVertices)
{
	m_dwFVF = D3DFVF_XYZ | D3DFVF_DIFFUSE  /*D3DFVF_PSIZE*/;

	m_iNumVertices = iNumVertices;

	if (FAILED(m_pGraphic_Device->CreateVertexBuffer(
		m_iNumVertices * sizeof(PARTICLE),
		D3DUSAGE_DYNAMIC | D3DUSAGE_POINTS | D3DUSAGE_WRITEONLY,
		m_dwFVF,
		D3DPOOL_DEFAULT,
		&m_pVB,
		0)))
		return E_FAIL;


	return NOERROR;
}

void CVtxBuffer::Free()
{
	m_pVB->Release();
	CComponent::Free();
}
