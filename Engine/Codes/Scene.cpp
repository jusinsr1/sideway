#include "..\Headers\Scene.h"
#include "Object_Manager.h"

CScene::CScene(LPDIRECT3DDEVICE9 pGraphic_Device)
	:m_pGraphic_Device(pGraphic_Device)
	,m_pObject_Manager(GET_INSTANCE(CObject_Manager))
{
	m_pObject_Manager->AddRef();
	m_pGraphic_Device->AddRef();
}

HRESULT CScene::Ready_Scene()
{
	return NOERROR;
}

_int CScene::Update_Scene(const _float & fTimeDelta)
{
	if(m_pObject_Manager==nullptr)
		return -1;
	return m_pObject_Manager->Update_GameObject_Manager(fTimeDelta);
}

_int CScene::LastUpdate_Scene(const _float & fTimeDelta)
{
	if (m_pObject_Manager == nullptr)
		return -1;
	return m_pObject_Manager->LastUpdate_GameObject_Manager(fTimeDelta);
}

void CScene::Render_Scene()
{
}

void CScene::Free()
{
	Safe_Release(m_pObject_Manager);
	Safe_Release(m_pGraphic_Device);
}
