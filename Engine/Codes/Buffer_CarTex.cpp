#include "..\Headers\Buffer_CarTex.h"

CBuffer_CarTex::CBuffer_CarTex(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CVIBuffer(pGraphic_Device)
{

}

CBuffer_CarTex::CBuffer_CarTex(const CBuffer_CarTex & rhs)
	: CVIBuffer(rhs)
{

}

HRESULT CBuffer_CarTex::Ready_VIBuffer()
{
	m_iNumVertices = 16;
	m_iStride = sizeof(VTXCUBETEX);
	m_dwFVF = D3DFVF_XYZ | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE3(0);
	
	m_iNumPolygons = 24;
	m_iPolygonSize = sizeof(POLYGON16);
	m_eFormat = D3DFMT_INDEX16;

	if (FAILED(CVIBuffer::Ready_VIBuffer()))
		return E_FAIL;

	VTXCUBETEX*		pVertices = nullptr;

	//m_pVB->Lock(0, 0, (void**)&pVertices, 0);

	//pVertices[0].vPosition = _vec3(-0.5f, 0.5f, -0.5f);
	//pVertices[0].vTexUV = pVertices[0].vPosition;

	//pVertices[1].vPosition = _vec3(0.5f, 0.5f, -0.5f);
	//pVertices[1].vTexUV = pVertices[1].vPosition;

	//pVertices[2].vPosition = _vec3(0.5f, -0.5f, -0.5f);
	//pVertices[2].vTexUV = pVertices[2].vPosition;

	//pVertices[3].vPosition = _vec3(-0.5f, -0.5f, -0.5f);
	//pVertices[3].vTexUV = pVertices[3].vPosition;

	//pVertices[4].vPosition = _vec3(-0.5f, 0.5f, 0.5f);
	//pVertices[4].vTexUV = pVertices[4].vPosition;

	//pVertices[5].vPosition = _vec3(0.5f, 0.5f, 0.5f);
	//pVertices[5].vTexUV = pVertices[5].vPosition;

	//pVertices[6].vPosition = _vec3(0.5f, -0.5f, 0.5f);
	//pVertices[6].vTexUV = pVertices[6].vPosition;

	//pVertices[7].vPosition = _vec3(-0.5f, -0.5f, 0.5f);
	//pVertices[7].vTexUV = pVertices[7].vPosition;
	//
	//m_pVB->Unlock();

	//for (size_t i = 0; i < m_iNumVertices; i++)
	//	pVertices[i].vTexUV = _vec2(0.f, 0.f);

	

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);

	pVertices[0].vPosition = _vec3(-0.25f, 0.5f, 0.25f);
	pVertices[1].vPosition = _vec3(0.25f, 0.5f, 0.25f);
	pVertices[2].vPosition = _vec3(0.25f, 0.15f, 0.25f);
	pVertices[3].vPosition = _vec3(0.5f, 0.15f, 0.25f);
	pVertices[4].vPosition = _vec3(0.5f,  -0.5f, 0.25f);
	pVertices[5].vPosition = _vec3(-0.5f, -0.5f, 0.25f);
	pVertices[6].vPosition = _vec3(-0.5f, 0.15f, 0.25f);
	pVertices[7].vPosition = _vec3(-0.25f, -0.15f, 0.25f);

	pVertices[8].vPosition = _vec3(-0.25f, 0.5f, -0.25f);
	pVertices[9].vPosition = _vec3(0.25f, 0.5f, -0.25f);
	pVertices[10].vPosition = _vec3(0.25f, 0.15f, -0.25f);
	pVertices[11].vPosition = _vec3(0.5f, 0.15f, -0.25f);
	pVertices[12].vPosition = _vec3(0.5f, -0.5f, -0.25f);
	pVertices[13].vPosition = _vec3(-0.5f, -0.5f, -0.25f);
	pVertices[14].vPosition = _vec3(-0.5f, 0.15f, -0.25f);
	pVertices[15].vPosition = _vec3(-0.25f, -0.15f, -0.25f);



	for (size_t i = 0; i < m_iNumVertices; i++)
	pVertices[i].vTexUV = pVertices[i].vPosition;

	m_pVB->Unlock();

	
	POLYGON16*		pIndices = nullptr;

	//m_pIB->Lock(0, 0, (void**)&pIndices, 0);

	//// +x
	//pIndices[0]._0 = 1; pIndices[0]._1 = 5; pIndices[0]._2 = 6;
	//pIndices[1]._0 = 1; pIndices[1]._1 = 6; pIndices[1]._2 = 2;

	//// -x
	//pIndices[2]._0 = 4; pIndices[2]._1 = 0; pIndices[2]._2 = 3;
	//pIndices[3]._0 = 4; pIndices[3]._1 = 3; pIndices[3]._2 = 7;

	//// +y
	//pIndices[4]._0 = 4; pIndices[4]._1 = 5; pIndices[4]._2 = 1;
	//pIndices[5]._0 = 4; pIndices[5]._1 = 1; pIndices[5]._2 = 0;

	//// -y
	//pIndices[6]._0 = 3; pIndices[6]._1 = 2; pIndices[6]._2 = 6;
	//pIndices[7]._0 = 3; pIndices[7]._1 = 6; pIndices[7]._2 = 7;

	//// +z
	//pIndices[8]._0 = 7; pIndices[8]._1 = 6; pIndices[8]._2 = 5;
	//pIndices[9]._0 = 7; pIndices[9]._1 = 5; pIndices[9]._2 = 4;

	//// -z
	//pIndices[10]._0 = 0; pIndices[10]._1 = 1; pIndices[10]._2 = 2;
	//pIndices[11]._0 = 0; pIndices[11]._1 = 2; pIndices[11]._2 = 3;

	//m_pIB->Unlock();


	m_pIB->Lock(0, 0, (void**)&pIndices, 0);
	// LEFT

	pIndices[0]._0 = 0;
	pIndices[0]._1 = 1;
	pIndices[0]._2 = 2;

	pIndices[1]._0 = 0;
	pIndices[1]._1 = 2;
	pIndices[1]._2 = 7;

	pIndices[2]._0 = 6;
	pIndices[2]._1 = 3;
	pIndices[2]._2 = 4;

	pIndices[3]._0 = 6;
	pIndices[3]._1 = 4;
	pIndices[3]._2 = 7;

	// BACK

	pIndices[4]._0 = 1;
	pIndices[4]._1 = 9;
	pIndices[4]._2 = 10;

	pIndices[5]._0 = 1;
	pIndices[5]._1 = 10;
	pIndices[5]._2 = 2;

	pIndices[6]._0 = 3;
	pIndices[6]._1 = 11;
	pIndices[6]._2 = 12;

	pIndices[7]._0 = 3;
	pIndices[7]._1 = 12;
	pIndices[7]._2 = 4;

	// TOP

	pIndices[8]._0 = 6;
	pIndices[8]._1 = 14;
	pIndices[8]._2 = 15;

	pIndices[9]._0 = 6;
	pIndices[9]._1 = 15;
	pIndices[9]._2 = 7;

	pIndices[10]._0 = 0;
	pIndices[10]._1 = 8;
	pIndices[10]._2 = 9;

	pIndices[11]._0 = 0;
	pIndices[11]._1 = 9;
	pIndices[11]._2 = 1;

	pIndices[12]._0 = 2;
	pIndices[12]._1 = 10;
	pIndices[12]._2 = 11;

	pIndices[13]._0 = 2;
	pIndices[13]._1 = 11;
	pIndices[13]._2 = 3;


	// FORNT

	pIndices[14]._0 = 8;
	pIndices[14]._1 = 0;
	pIndices[14]._2 = 7;

	pIndices[15]._0 = 8;
	pIndices[15]._1 = 7;
	pIndices[15]._2 = 15;

	pIndices[16]._0 = 14;
	pIndices[16]._1 = 6;
	pIndices[16]._2 = 5;

	pIndices[17]._0 = 14;
	pIndices[17]._1 = 5;
	pIndices[17]._2 = 13;

	// BOTTOM

	pIndices[18]._0 = 13;
	pIndices[18]._1 = 12;
	pIndices[18]._2 = 4;

	pIndices[19]._0 = 13;
	pIndices[19]._1 = 4;
	pIndices[19]._2 = 5;


	// RIGHT

	pIndices[20]._0 = 8;
	pIndices[20]._1 = 9;
	pIndices[20]._2 = 10;

	pIndices[21]._0 = 8;
	pIndices[21]._1 = 10;
	pIndices[21]._2 = 15;

	pIndices[22]._0 = 14;
	pIndices[22]._1 = 11;
	pIndices[22]._2 = 12;

	pIndices[23]._0 = 14;
	pIndices[23]._1 = 12;
	pIndices[23]._2 = 13;

	m_pIB->Unlock();


	return NOERROR;
}

void CBuffer_CarTex::Render_VIBuffer()
{
	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, m_iStride);
	m_pGraphic_Device->SetFVF(m_dwFVF);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_iNumVertices, 0, m_iNumPolygons);
}


CBuffer_CarTex * CBuffer_CarTex::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CBuffer_CarTex*	pInstance = new CBuffer_CarTex(pGraphic_Device);

	if (FAILED(pInstance->Ready_VIBuffer()))
	{
		MessageBox(0, L"CBuffer_CarTex Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}

	return pInstance;
}


CComponent * CBuffer_CarTex::Clone_Component()
{
	return new CBuffer_CarTex(*this);
}

void CBuffer_CarTex::Free()
{
	CVIBuffer::Free();
}
