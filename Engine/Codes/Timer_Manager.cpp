#include "..\Headers\Timer_Manager.h"
#include "Timer.h"

_IMPLEMENT_SINGLETON(CTimer_Manager)

CTimer_Manager::CTimer_Manager()
{
}

HRESULT CTimer_Manager::Add_Timer(const _tchar * pTimerTag)
{
	CTimer*		pTimer = Find_Timer(pTimerTag);
	if (pTimer != nullptr)
		return E_FAIL;

	pTimer = CTimer::Create();
	if (pTimer == nullptr)
		return E_FAIL;
	
	m_mapTimers.insert({ pTimerTag,pTimer });
	return NOERROR;
}

_float CTimer_Manager::Get_TimeDelta(const _tchar * pTimerTag)
{
	CTimer*		pTimer = Find_Timer(pTimerTag);
	if (pTimer == nullptr)
		return 0.f;

	return pTimer->Get_TimeDelta();
}

CTimer * CTimer_Manager::Find_Timer(const _tchar * pTimerTag)
{
	auto& iter = find_if(m_mapTimers.begin(), m_mapTimers.end(), CFinder_Tag(pTimerTag));
	if (iter == m_mapTimers.end())
		return nullptr;

	return iter->second;
}

void CTimer_Manager::Free()
{
	_ulong		dwRefCnt = 0;
	for (auto& Pair : m_mapTimers)
	{
		if (dwRefCnt = Safe_Release(Pair.second))
			_MSG_BOX("CTimer Release Failed");
	}
	m_mapTimers.clear();
}
