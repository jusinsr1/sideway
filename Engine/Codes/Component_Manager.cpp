#include "..\Headers\Component_Manager.h"

_IMPLEMENT_SINGLETON(CComponent_Manager)

CComponent_Manager::CComponent_Manager()
{
}

HRESULT CComponent_Manager::Reserve_Component_Manager(const _uint & iNumScene)
{
	if (m_pMapPrototype != nullptr)
		return E_FAIL;

	m_pMapPrototype = new MAPPROTOTYPE[iNumScene];

	m_iNumScene = iNumScene;

	return NOERROR;
}

HRESULT CComponent_Manager::Add_Prototype_Component(const _uint & iSceneID, const _tchar* pComponentTag, CComponent * pComponent)
{
	if (pComponent == nullptr)
		return E_FAIL;
	if (m_pMapPrototype == nullptr)
		return E_FAIL;
	if (m_iNumScene <= iSceneID)
		return E_FAIL;
	if (Find_Component(iSceneID, pComponentTag) != nullptr)
		return E_FAIL;

	m_pMapPrototype[iSceneID].insert({ pComponentTag, pComponent });

	return NOERROR;
}

CComponent * CComponent_Manager::Clone_Component(const _uint & iSceneID, const _tchar * pComponentTag)
{
	CComponent* pPrototype = Find_Component(iSceneID, pComponentTag);
	if (pPrototype == nullptr)
		return nullptr;
	CComponent* pClone = pPrototype->Clone_Component();

	if (pClone == nullptr)
		return nullptr;

	return pClone;
}

CComponent * CComponent_Manager::Find_Component(const _uint & iSceneID, const _tchar* pComponentTag)
{
	auto& iter = find_if(m_pMapPrototype[iSceneID].begin(), m_pMapPrototype[iSceneID].end(), CFinder_Tag(pComponentTag));
	if (iter == m_pMapPrototype[iSceneID].end())
		return nullptr;
	return iter->second;
}

void CComponent_Manager::Free()
{
	for (size_t i = 0; i < m_iNumScene; i++)
	{
		for (auto& Pair : m_pMapPrototype[i])
			Safe_Release(Pair.second);

		m_pMapPrototype[i].clear();
	}

	Safe_Delete_Array(m_pMapPrototype);
}