#include "..\Headers\System.h"
#include "Frame_Manager.h"
#include "Timer_Manager.h"

_IMPLEMENT_SINGLETON(CSystem)

CSystem::CSystem()
	:m_pTimer_Manager(GET_INSTANCE(CTimer_Manager))
	,m_pFrame_Manager(GET_INSTANCE(CFrame_Manager))
{
	m_pTimer_Manager->AddRef();
	m_pFrame_Manager->AddRef();
}

HRESULT CSystem::Add_Timer(const _tchar * pTimerTag)
{
	if (m_pTimer_Manager == nullptr)
		return E_FAIL;
	return m_pTimer_Manager->Add_Timer(pTimerTag);
}

_float CSystem::Get_TimeDelta(const _tchar * pTimerTag)
{
	if (m_pTimer_Manager == nullptr)
		return 0.0f;

	return m_pTimer_Manager->Get_TimeDelta(pTimerTag);
}

HRESULT CSystem::Add_Frame(const _tchar * pFrameTag, const _float & fCallCnt)
{
	if (m_pFrame_Manager == nullptr)
		return E_FAIL;

	return m_pFrame_Manager->Add_Frame(pFrameTag, fCallCnt);
}

_bool CSystem::Permit_Call(const _tchar * pFrameTag, const _float & fTimeDelta)
{
	if (m_pFrame_Manager == nullptr)
		return false;

	return m_pFrame_Manager->Permit_Call(pFrameTag, fTimeDelta);
}

void CSystem::Free()
{
	Safe_Release(m_pFrame_Manager);
	Safe_Release(m_pTimer_Manager);
}
